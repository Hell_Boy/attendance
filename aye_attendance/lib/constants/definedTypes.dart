



import 'package:aye_attendance/models/sharedAccess.dart';
import 'package:aye_attendance/models/userModel.dart';
import 'package:flutter/material.dart';

typedef functionCallback = void Function();
typedef responseCallback = Function(Map<String,dynamic> response);
typedef stringCallback = void Function(String value);
typedef booleanCallback = void Function(bool value);
typedef dropdownCallback = void Function(int index, String value);
//typedef visitorsListCallback = void Function(List<VisitorInfo> list);
typedef accessListCallback = void Function(Map<String,SharedAccess> access);
typedef responseStringCallback = Function(String res);
//typedef historyListCallback = void Function(List<HistoryModel> list);
typedef intCallback = void Function(int count);
typedef userCallback = void Function(UserModel user);
typedef widgetCallback = Widget Function(GlobalKey<ScaffoldState> scaffold);
