
// region String Constants
const TAG_FILE_TO_JSON = 'File To Json';
const TAG_TOKEN = 'Token';
const TAG_ROUTER = 'Router';
const TAG_LOGIN = 'Login';
const TAG_SIGNUP = 'Signup';
const TAG_FIREBASE_REQUEST = 'Firebase Request';
const TAG_QUERYTYPE = 'Query Type';
const TAG_LEAVES = 'Leaves';
const TAG_ATTENDANCE = 'Attendance';
const TITLE_WEBSCREEN = 'Web Screen';
const TITLE_HOME_SCREEN = 'Home';


const MOBILE = 'Mobile';
const NUMBER = 'Number';
const OTP = 'Otp';
const LOGIN = 'Login';
const ATTENDANCE = 'Attendance';
const PUNCH = 'Punch';
const LEAVES = 'Leaves';
const HELP = 'Help';
const SIGN_IN_SUCCESSFUL = 'Sign in successful';
const SIGN_IN_FAILED = 'Sign in failed';
const RECEIVED_PHONE_CREDENTIALS = 'Phone credentials received';
const PHONE_VERIFIFCATION_FAILED = 'Phone verification failed.';
const CHECK = 'Check';
const CHECK_OTP = CHECK+' for '+OTP;
const COUNTRY_CODE_INDIA = '91';
const ACCOUNT_VERIFICATION_MSG = 'Verifing account.';
const PLEASE_WAIT = 'Please wait.';
const MSG_SIGN_UP_SUCCESSFULL = 'Signup successfully.';
const MSG_SIGN_UP_FAILED = 'Signup failed.';



const String WEBSITE_URL = "http://www.google.com";
const String FEEDBACK_URL = "http://www.google.com";
const String LOCATION_URL = "https://www.google.com/maps/search/?api=1&query=28.5701764,77.0219015";


const String welcomeToAyeFinance = "<center>Welcome to <a href=\"http://www.google.com\">Company!</a></center>";

const String singleLineLoremIpsum = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
// endregion


// region Boolean
const LOGS_ENABLED = true;
// endregion