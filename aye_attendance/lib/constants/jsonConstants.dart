
// region Title, Image and Description

import 'constant.dart';

final List<Map<String, dynamic>> jsonConstant   = [
    {
        "title": "Humanoid and puppy robot",
        "image_url": "https://databox.com/wp-content/themes/databox/inc/img/signup/signup-1.png",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Fish and Ball",
        "image_url": "https://static.wixstatic.com/media/203da4_737db61314ba4181bbe6f6dcf54b86fc~mv2.jpg/v1/fill/w_610,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_737db61314ba4181bbe6f6dcf54b86fc~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Hoberman Sphere",
        "image_url": "https://static.wixstatic.com/media/203da4_2f735165d6f341c0a43ed7ca9dd43449~mv2.jpg/v1/fill/w_651,h_722,al_c,q_90,usm_0.66_1.00_0.01/203da4_2f735165d6f341c0a43ed7ca9dd43449~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "3D Printer",
        "image_url": "https://static.wixstatic.com/media/203da4_ff31f0715920435589958eeff03a876a~mv2.jpg/v1/fill/w_574,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_ff31f0715920435589958eeff03a876a~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Robotic Printer",
        "image_url": "https://static.wixstatic.com/media/203da4_5cd721d9043b448db42947e637570ac2~mv2.jpg/v1/fill/w_1100,h_722,al_c,q_90,usm_0.66_1.00_0.01/203da4_5cd721d9043b448db42947e637570ac2~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Steam Engine",
        "image_url": "https://static.wixstatic.com/media/203da4_bc4b17b9419245dd801a661de3671448~mv2.jpg/v1/fill/w_393,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_bc4b17b9419245dd801a661de3671448~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Magnetic Gun",
        "image_url": "https://static.wixstatic.com/media/203da4_0bc5e456a7b04e0883e4c47ac4c3e029~mv2.jpg/v1/fill/w_212,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_0bc5e456a7b04e0883e4c47ac4c3e029~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Lift Model",
        "image_url": "https://static.wixstatic.com/media/203da4_d384646167b2468baa47c89c94eb7e32~mv2.jpg/v1/fill/w_464,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_d384646167b2468baa47c89c94eb7e32~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Planetarium Model",
        "image_url": "https://static.wixstatic.com/media/203da4_ea37a27134e741aea3510a385075f48d~mv2.jpg/v1/fill/w_627,h_722,al_c,q_90,usm_0.66_1.00_0.01/203da4_ea37a27134e741aea3510a385075f48d~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Balancing Eagles",
        "image_url": "https://static.wixstatic.com/media/203da4_dde1c2b665284c92950cc911fabe95c8~mv2.jpg/v1/fill/w_960,h_592,al_c,q_90/203da4_dde1c2b665284c92950cc911fabe95c8~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    },
    {
        "title": "Infinity Mirror",
        "image_url": "https://static.wixstatic.com/media/203da4_1daab43275c94eeba6b53da8e95ff47d~mv2.jpg/v1/fill/w_589,h_739,al_c,q_90,usm_0.66_1.00_0.01/203da4_1daab43275c94eeba6b53da8e95ff47d~mv2.webp",
        "description": "McClintock's eye for detail certainly helped narrow the whereabouts of lorem ipsum's origin, however, the “how and when” still remain something of a mystery, with competing theories and timelines."
    }

];

// endregion

// region DateTime and Event

final List<Map<String, dynamic>> dateTimeJsonConstant = [
    {
        "title": "Sick leave",
        "type" : 101,
        "code" : 'SL',
        "timestamp": 1573620188,
        "description": singleLineLoremIpsum
    },
    {
        "title": "Casual leave",
        "type" : 102,
        "code" : 'CL',
        "timestamp": 1573620188,
        "description": singleLineLoremIpsum
    },
    {
        "title": "Sick leave",
        "type" : 101,
        "code" : 'SL',
        "timestamp": 1573620188,
        "description": singleLineLoremIpsum
    },
    {
        "title": "Restricted holidays",
        "type" : 103,
        "code" : 'RH',
        "timestamp": 1573620188,
        "description": singleLineLoremIpsum
    },

];
// endregion


// region Leave Structure

final Map<String, dynamic> leavesStructure =
{
    "rh_id" : 'leave_001',
    "rh_title" : 'Restricted Holidays',
    "rh_initial" : 8,
    "rh_final" : 7,
    'sl_id' : 'leave_002',
    'sl_title' : 'Sick Leaves',
    "sl_initial" : 9,
    "sl_final" : 5,
    'cl_id' : 'leave_003',
    'cl_title' : 'Casual Leaves',
    "cl_initial" : 10,
    "cl_final" : 6
};
// endregion


// region LeaveDay

final Map<String, dynamic> jsonConstantLeaveDay = {
    'id' : 'aaaa',
    'timestamp' : 1574035200,
    'in_time' : 1574137088,
    'out_time' : 1574139537,
    'uid' : 'uuuu',
    'guard_id': 'guard'

};

// endregion


// region Leave Types

final List<Map<String, dynamic>> leaveConfig = [

    {
    'id' : 'leave_001',
        'title' : 'Restricted_Holidays',
        'initial' : 10,
        'allowed' : [
            'XX00',
            'XX01',
            'XX10',
            'XX11',
        ],
    },
    {
        'id' : 'leave_002',
        'title' : 'Casual Leave',
        'initial' : 10,
        'allowed' : [
            'XX00',
            'XX01',
            'XX10',
            'XX11',
        ],
    },
    {
        'id' : 'leave_003',
        'title' : 'Sick Leave',
        'initial' : 10,
        'allowed' : [
            'XX00',
            'XX01',
            'XX10',
            'XX11',
        ],
    },
];
// endregion