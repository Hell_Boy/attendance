
import 'SlidingEdgeClipper.dart';
import 'BottomInvertedOvalClipper.dart';
import 'BottomOvalClipper.dart';
import 'BottomWaveClipper.dart';
import 'InvertRoundedCornerClipper.dart';
import 'PointedMultipleEdgeClipper.dart';
import 'RoundedMultipleCurveClipper.dart';
import 'SlidingEdgeClipper2.dart';
import 'TriangleClipper.dart';

enum Clip{
  bottom_oval_bottom,
  bottom_inverted_oval_bottom,
  bottom_wave_clipper,
  triangle_clipper,
  invert_rounded_corner_clipper,
  pointed_multiple_edge_clipper,
  rounded_multiple_curve_clipper,
  sliding_edge_clipper,
  sliding_edge_clipper2,

}

ClipType(type){
  switch(type){

    case Clip.bottom_wave_clipper:
      return BottomWaveClipper();

    case Clip.bottom_oval_bottom:
      return BottomOvalClipper();

    case Clip.bottom_inverted_oval_bottom:
      return BottomInvertedOvalClipper();

    case Clip.triangle_clipper:
      return TriangleClipper();

    case Clip.invert_rounded_corner_clipper:
      return InvertRoundedCornerClipper();

    case Clip.pointed_multiple_edge_clipper:
      return PointedMultipleEdgeClipper();

    case Clip.rounded_multiple_curve_clipper:
      return RoundedMultipleCurveClipper();

    case Clip.sliding_edge_clipper :
      return SlidingEdgeClipper();

    case Clip.sliding_edge_clipper2 :
      return SlidingEdgeClipper2();
  }
}