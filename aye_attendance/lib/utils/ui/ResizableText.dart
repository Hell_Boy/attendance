import 'package:auto_size_text/auto_size_text.dart' as autoSizeText;
import 'package:flutter/cupertino.dart';

// @required : auto_size_text: ^2.1.0
// ignore: non_constant_identifier_names
Widget ResizableText(
    {
      @required String data,
      @required TextStyle style
    }){

  return autoSizeText.AutoSizeText(
    data,
    style: style,
    minFontSize: 13,
    maxLines: 4,
    overflow: TextOverflow.ellipsis,
  );
}