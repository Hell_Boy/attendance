
import 'package:aye_attendance/constants/definedTypes.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
Widget DropdownUtil({
  @required String selectedValue,
  @required List<String> options,
  @required stringCallback onSelected}){
  return DropdownButton<String>(
    value: selectedValue,
    icon: Icon(Icons.arrow_downward),
    iconSize: 24,
    elevation: 16,
    style: TextStyle(
        color: Colors.deepPurple
    ),
    underline: Container(
      height: 2,
      color: Colors.deepPurpleAccent,
    ),
    onChanged: (String newValue) {
      onSelected(newValue);
    },
    items: options
        .map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    })
        .toList(),
  );
}