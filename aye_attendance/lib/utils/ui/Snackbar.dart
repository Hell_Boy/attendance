
import 'package:flutter/material.dart';

showSnackBar({
  @required GlobalKey<ScaffoldState> scaffoldKey,
  @required String msg}){
  scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg),));
}