
import 'package:flutter/material.dart';


// ignore: must_be_immutable
class FullScreenDialog extends StatefulWidget {
  String title;
  Widget child;

  FullScreenDialog({Key key, @required this.title, @required this.child,}) : super(key:key);

  @override
  _FullScreenDialogState createState() => _FullScreenDialogState();
}

class _FullScreenDialogState extends State<FullScreenDialog> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(title : Text(widget.title, style: Theme.of(context).textTheme.headline)),
        body : _body()
    );
  }

  Widget _body(){
    return Container(
      child: widget.child,
    );
  }
}
