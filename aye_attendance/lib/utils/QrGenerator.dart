
import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/ui/Progress.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/material.dart';

// @required  qr_flutter: ^3.0.1

class QrGenerator{

  Widget generate({
    @required String data
}){
    try {

      return QrImage(
        data: data,
        version: QrVersions.auto,
        size: Store.dimension.width/4,
      );

    }catch(e){
      return Center(

        child: Progress(text: PLEASE_WAIT),
      );
    }
  }
}