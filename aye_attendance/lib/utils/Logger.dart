
import 'package:aye_attendance/constants/constant.dart';
import 'package:flutter/cupertino.dart';

class Log{

  String tag;
  String message;


  static d({
    @required tag,
    @required message
  }){
    if(LOGS_ENABLED)
      print('${tag} : ${message}');
  }

}