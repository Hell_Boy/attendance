
import 'dart:convert';
import 'dart:io';


import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/definedTypes.dart';
import 'package:flutter/cupertino.dart';

import 'Logger.dart';

class FileToJson{

   String path;
   responseCallback response;

   FileToJson({
     @required this.path,
     @required this.response}){
     readFile();
   }

   readFile(){
     new File(path).readAsString().then((String contents) {
       Log.d(tag: TAG_FILE_TO_JSON, message: 'File data: $contents');

       response(json.decode(contents));
     });
   }

}