

import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/definedTypes.dart';
import 'package:aye_attendance/constants/firebaseConstant.dart';
import 'package:aye_attendance/models/sharedAccess.dart';
import 'package:aye_attendance/models/userModel.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:flutter/material.dart';

class QueryType{

  userProfile({
    @required String userId,
    @required userCallback user
  }){
    FirebaseRequest.getData(path: FIREBASE_USER+userId, onResponse: (response){

      try{
        user(UserModel.fromJson(response));
      }catch(e){
        Log.d(tag: QueryType, message: 'User Profile : $e');
        user(null);
      }
    });
  }

  signup({
    @required String userId,
    @required UserModel user,
    @required booleanCallback onResponse
  }){
    FirebaseRequest.put(path: FIREBASE_USER+userId, data: user.toJson(), onResponse: (response){
      onResponse(response);
    });
  }

  sharedAccess({
    @required accessListCallback accessList,
  }){
    FirebaseRequest.getData(path: FIREBASE_ACCESS, onResponse: (response){

      try{
        Log.d(tag: QueryType, message: 'Shared Access : ${response.toString()}');
        Map<String, SharedAccess> _access = {};
        response.forEach((key, value){
          _access.putIfAbsent(key, ()=> SharedAccess.fromJson(value.cast<String,dynamic>()));
        });
        accessList(_access);
//        _access.putIfAbsent(key, ()=> SharedAccess.fromJson(value.cast<String,dynamic>()));
      }catch(e){
        Log.d(tag: QueryType, message: 'Shared Access : $e');
        accessList(null);
      }
    });
  }

}

class FirebaseRequest{


  static getData({
    @required String path,
    @required responseCallback onResponse
  }){

    databaseReference.child(path).once().then((data){

      if(data.value != null)
        onResponse(data.value.cast<String,dynamic>());
      else
        onResponse(null);

    }).catchError((e){
      onResponse(null);
      Log.d(tag: TAG_FIREBASE_REQUEST, message: 'Get Exception : $e');
    });
  }


  static put({
    @required String path,
    @required Map<String,dynamic> data,
    @required booleanCallback onResponse
  }){

    bool completed = false;

    databaseReference.child(path).set(data).whenComplete((){
      completed = true;
      onResponse(completed);
    }).catchError((e){
      onResponse(completed);
      Log.d(tag: TAG_FIREBASE_REQUEST, message: 'Put Exception : $e');
    });
  }



  static generateKey({
    @required String path,
    @required stringCallback key
  }){
    key(databaseReference.child(path).push().key);
  }
}