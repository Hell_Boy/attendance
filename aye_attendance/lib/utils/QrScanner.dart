

import 'package:aye_attendance/constants/definedTypes.dart';
import 'package:qrcode_reader/qrcode_reader.dart';

// @required   qrcode_reader: ^0.4.4

class QrScanner{


  scan(responseStringCallback resp){
    Future<String> futureString = new QRCodeReader()
        .setAutoFocusIntervalInMs(200) // default 5000
        .setForceAutoFocus(true) // default false
        .setTorchEnabled(true) // default false
        .setHandlePermissions(true) // default true
        .setExecuteAfterPermissionGranted(true) // default true
        .scan();

    futureString.then((value){
      resp(value);
    });

  }
}