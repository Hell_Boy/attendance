
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart' as launcher;
import 'dart:async';


// @Required  :   url_launcher: ^5.2.5

class Launcher{

  launch({@required String element}) async {
    if (await launcher.canLaunch(element)) {
      await launcher.launch(element);
    } else {
      throw 'Could not launch $element';
    }
  }
}