// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel(
      access: json['access'] as String,
      access_name: json['access_name'] as String,
      uid: json['uid'] as String,
      name: json['name'] as String,
      email: json['email'] as String,
      token: json['token'] as String,
      webSessionId: json['webSessionId'] as String,
      dob: json['dob'] as String,
      empId: json['empId'] as String,
      check_in: json['check_in'] as bool,
      check_in_id: json['check_in_id'] as String,
      phone: json['phone'] as String,
      attendance_calendar: json['attendance_calendar'] as String);
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'access': instance.access,
      'access_name': instance.access_name,
      'uid': instance.uid,
      'name': instance.name,
      'email': instance.email,
      'token': instance.token,
      'webSessionId': instance.webSessionId,
      'empId': instance.empId,
      'dob': instance.dob,
      'check_in': instance.check_in,
      'check_in_id': instance.check_in_id,
      'phone': instance.phone,
      'attendance_calendar': instance.attendance_calendar
    };
