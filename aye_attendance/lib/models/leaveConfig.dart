import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/material.dart';


/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'leaveConfig.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
/// flutter pub run build_runner build


@JsonSerializable()

class LeaveConfig {

  String id;
  String title;
  int initial;
  List<String> allowed;


  LeaveConfig(this.id, this.title, this.initial, this.allowed);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory LeaveConfig.fromJson(Map<String, dynamic> json) => _$LeaveConfigFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$LeaveConfigToJson(this);
}