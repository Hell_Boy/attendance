// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userLeaveModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserLeaveModel _$UserLeaveModelFromJson(Map<String, dynamic> json) {
  return UserLeaveModel(
      json['id'] as String,
      json['timestamp'] as int,
      json['in_time'] as int,
      json['out_time'] as int,
      json['uid'] as String,
      json['guard_id'] as String);
}

Map<String, dynamic> _$UserLeaveModelToJson(UserLeaveModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'timestamp': instance.timestamp,
      'in_time': instance.in_time,
      'out_time': instance.out_time,
      'uid': instance.uid,
      'guard_id': instance.guard_id
    };
