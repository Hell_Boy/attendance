// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sharedAccess.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SharedAccess _$SharedAccessFromJson(Map<String, dynamic> json) {
  return SharedAccess(json['code'] as String, json['name'] as String,
      (json['screens'] as List)?.map((e) => e as String)?.toList());
}

Map<String, dynamic> _$SharedAccessToJson(SharedAccess instance) =>
    <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'screens': instance.screens
    };
