// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leaveConfig.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LeaveConfig _$LeaveConfigFromJson(Map<String, dynamic> json) {
  return LeaveConfig(
      json['id'] as String,
      json['title'] as String,
      json['initial'] as int,
      (json['allowed'] as List)?.map((e) => e as String)?.toList());
}

Map<String, dynamic> _$LeaveConfigToJson(LeaveConfig instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'initial': instance.initial,
      'allowed': instance.allowed
    };
