// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leaveModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LeaveModel _$LeaveModelFromJson(Map<String, dynamic> json) {
  return LeaveModel(
      json['title'] as String,
      json['code'] as String,
      json['type'] as int,
      json['timestamp'] as int,
      json['description'] as String);
}

Map<String, dynamic> _$LeaveModelToJson(LeaveModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'code': instance.code,
      'type': instance.type,
      'timestamp': instance.timestamp,
      'description': instance.description
    };
