import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/definedTypes.dart';
import 'package:aye_attendance/constants/jsonConstants.dart';
import 'package:aye_attendance/constants/ui/fonts.dart';
import 'package:aye_attendance/models/leaveConfig.dart';
import 'package:aye_attendance/models/userLeaveModel.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/DateFormatter.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:aye_attendance/utils/ui/Calendar.dart';
import 'package:aye_attendance/utils/ui/Dropdown.dart';
import 'package:aye_attendance/utils/ui/FullScreenDialog.dart';
import 'package:flutter/material.dart';

class AttendanceScreen extends StatefulWidget {
  @override
  _AttendanceScreenState createState() => _AttendanceScreenState();
}

class _AttendanceScreenState extends State<AttendanceScreen> {

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(title : Text("Attendance", style: Theme.of(context).textTheme.headline)),
        body : _body()
    );
  }

  Widget _body(){


    return Container(
      child: Calendar(
        onDaySelectedLong: (String value) {

          String date = ddMMyyyy_DateFormat(value: dateFromTimestamp(timestamp: int.parse(value)));
          Log.d(tag: TAG_ATTENDANCE, message: value);
//          showBottomSheet(value);
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullScreenDialog(
                title: date,
                child: Regularise_Or_Leave(),
              ),fullscreenDialog: true)
          );
        },),
    );
  }

}





class Regularise_Or_Leave extends StatefulWidget {
  @override
  _Regularise_Or_LeaveState createState() => _Regularise_Or_LeaveState();
}

class _Regularise_Or_LeaveState extends State<Regularise_Or_Leave> {

  int _currentIndex = 1;

  String _selectedLeave;

  List<GroupModel> _group = [
    GroupModel(
      text: "Regularise",
      index: 1,
    ),
    GroupModel(
      text: "Leave",
      index: 2,
    ),
  ];

  List<LeaveConfig> _leaves = [];

  String remark;

  @override
  void initState() {
    super.initState();

    List<LeaveConfig> _temp = [];

    leaveConfig.forEach((value){
      _temp.add(LeaveConfig.fromJson(value));
    });

    _selectedLeave = _temp.elementAt(0).title;

    Store.leaveConfig = _temp;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body : SingleChildScrollView(child: _body())
    );
  }

  Widget _body(){
    return Container(
      height: Store.dimension.height,
      child: _leave(),
    );
  }

  Widget _leave(){

    UserLeaveModel leaveModel = UserLeaveModel.fromJson(jsonConstantLeaveDay);

    var textTheme = Theme.of(context).textTheme.title;

    return Column(
      children: <Widget>[
        Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: <Widget>[
              Text('Intime', style: textTheme.copyWith(fontFamily: FONT_FAMILY_OXYGEN),),
              Text('  : '+hhmma_TimeFormat(value: dateFromTimestamp(timestamp: leaveModel.in_time*1000)), style: textTheme,),
            ],),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: <Widget>[
              Text('Outime', style: textTheme.copyWith(fontFamily: FONT_FAMILY_OXYGEN),),
              Text(': '+hhmma_TimeFormat(value: dateFromTimestamp(timestamp: leaveModel.out_time*1000)), style: textTheme,),
            ],),
          ),

        ],),

        Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    children: _group
                        .map((item) => Flexible(
                      child: RadioListTile(
                        groupValue: _currentIndex,
                        title: Text("${item.text}", style: Theme.of(context).textTheme.title,),
                        value: item.index,
                        onChanged: (val) {
                          Log.d(tag: TAG_ATTENDANCE, message: 'Option Selected : $val');
                          setState(() {
                            _currentIndex = val;
                          });
                        },
                      ),
                    ))
                        .toList(),
                  ),
                ),

                Container(
//                  padding: const EdgeInsets.all(8.0),
                  child: _currentIndex == 1 ? _regulariseWidget() : _leaveWidget(),
                ),
                RaisedButton(onPressed: ((){}), child: Text('Submit'), color: Colors.green, textColor: Colors.white,)
              ],
            ))
      ],
    );
  }

  Widget _regulariseWidget(){


    return _remarks(remark: ((value){
      setState(() {
        remark = value;
      });
    }));
  }

  Widget _leaveWidget(){

    return Column(
      children: <Widget>[
        DropdownUtil(
            selectedValue : _selectedLeave,
            options: Store.leaveConfig.map((value){
              return value.title;
            }).toList(),
            onSelected: ((value){
              setState(() {
                _selectedLeave = value;
              });
            })
        ),

        _remarks(remark: ((value){
          setState(() {
            remark = value;
          });
        }))
      ],
    );
  }

  Widget _remarks({@required stringCallback remark}){

    return Container(
//            height: Store.dimension.height/6,
        margin: const EdgeInsets.all(8.0),
    decoration: BoxDecoration(
    border: Border.all(color: Colors.black, width: 1.0),
    borderRadius: BorderRadius.circular(8.0)),
    padding: const EdgeInsets.all(8.0),
    alignment: Alignment.bottomLeft,
    child: TextField(
    maxLines: 3,
    decoration: InputDecoration(border: InputBorder.none, hintText: 'Remarks*'),
      onChanged: ((value){
        remark(value);
      }),
    )
    );
  }

}



class GroupModel {
  String text;
  int index;

  GroupModel({
    @required this.text,
    @required this.index,
  });
}
