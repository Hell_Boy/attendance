
import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/ui/fonts.dart';
import 'package:aye_attendance/models/userModel.dart';
import 'package:aye_attendance/routes/router.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:aye_attendance/utils/QueryType.dart';
import 'package:aye_attendance/utils/token.dart';
import 'package:aye_attendance/utils/ui/Snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {

  final GlobalKey<FormState> _signupForm = new GlobalKey();
//  final FocusNode _nameFocus = FocusNode();
  final FocusNode _empIdFocus = FocusNode();
  final FocusNode _dobFocus = FocusNode();

  String __dobPreviousValue = '';
  TextEditingController _nameController, _empIdController,_dobController;

  final GlobalKey<ScaffoldState> _scaffold = new GlobalKey();

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _empIdController = TextEditingController();
    _dobController = TextEditingController();
  }

@override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _empIdController.dispose();
    _dobController.dispose();

    _empIdFocus.dispose();
    _dobFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffold,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(title : Text("Signup", style: Theme.of(context).textTheme.display1.copyWith(letterSpacing: 1.5))),
        body : _body()
    );
  }

  Widget _body(){
    return Container(
      padding: const EdgeInsets.all(8.0),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        child: Form(
          key: _signupForm,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _nameController,
                  style: TextStyle(fontFamily: FONT_FAMILY_OXYGEN),
                  showCursor: false,
                  maxLines: 1,
                  decoration: InputDecoration(
                      labelText: 'Name',
                      labelStyle: Theme.of(context).textTheme.title.copyWith(color: Colors.black54)
                  ),

                  onFieldSubmitted: ((value){
                    FocusScope.of(context).requestFocus(_empIdFocus);
                  }),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _empIdController,
                  showCursor: false,
                  focusNode: _empIdFocus,
                  maxLines: 1,
                  style: TextStyle(fontFamily: FONT_FAMILY_OXYGEN),
                  decoration: InputDecoration(
                      labelText: 'Employee Id',
                      labelStyle: Theme.of(context).textTheme.title.copyWith(color: Colors.black54)
                  ),

                  onFieldSubmitted: ((value){
                    _empIdFocus.unfocus();
                    FocusScope.of(context).requestFocus(_dobFocus);
                  }),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _dobController,
                  style: TextStyle(fontFamily: FONT_FAMILY_OXYGEN),
                  maxLength: 10,
                  showCursor: false,
                  focusNode: _dobFocus,
                  maxLines: 1,
                  keyboardType: TextInputType.datetime,
                  onChanged: ((value){

                      if(__dobPreviousValue.length < _dobController.text.length) {
                        if(value.length == 2  || value.length == 5)
                          _dobController.text = value+'/';
//                        _dobController.value = TextEditingValue(text: value+'/');
                        _dobController.selection = TextSelection.collapsed(offset: _dobController.text.length, affinity: TextAffinity.upstream);

                      }
                      __dobPreviousValue = _dobController.text;

                  }),
                  decoration: InputDecoration(
                    isDense: true,
                    labelText: 'Date of Birth',
                    hintText: 'dd/mm/yyyy',
                    counterText: '',
                    labelStyle: Theme.of(context).textTheme.title.copyWith(color: Colors.black54),
                  ),

                  onFieldSubmitted: ((value){

                    _dobFocus.unfocus();
                  }),
                ),
              ),

              Container(
                margin: const EdgeInsets.only(top: 18),
                padding: const EdgeInsets.all(8.0),
                child: FloatingActionButton.extended(onPressed: ((){

//                  Todo : Validate Submitted data
                  _submit();
                }), label: Text('Submit')),
              ),


            ],
          ),
        ),
      ),
    );
  }

  _submit(){

    showSnackBar(scaffoldKey: _scaffold, msg: PLEASE_WAIT);

    var user = UserModel(
      access: Store.accessEmployee.code,
      access_name: Store.accessEmployee.name,
      uid: Store.authUser.uid,
      name: _nameController.text,
      empId: _empIdController.text,
      dob: _dobController.text,
      phone: Store.authUser.phoneNumber,
      attendance_calendar: 'calendar_'+Store.authUser.uid
    );

    QueryType().signup(userId: Store.authUser.uid, user: user, onResponse: ((response){
      if(response){
//        Store.userModel = user;
        Navigator.pushReplacementNamed(context, RouterPath(Path.LoginScreen));
        showSnackBar(scaffoldKey: _scaffold, msg: MSG_SIGN_UP_SUCCESSFULL);
      }else{
        showSnackBar(scaffoldKey: _scaffold, msg: MSG_SIGN_UP_FAILED);
      }
    }));
  }
}
