

import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/firebaseConstant.dart';
import 'package:aye_attendance/constants/ui/fonts.dart';
import 'package:aye_attendance/models/userModel.dart';
import 'package:aye_attendance/routes/router.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/Launcher.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:aye_attendance/utils/QueryType.dart';
import 'package:aye_attendance/utils/ui/HtmlText.dart';
import 'package:aye_attendance/utils/ui/SetScreenDimension.dart';
import 'package:aye_attendance/utils/ui/clippers/ImageClipper.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';


final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController _controller;
  String _message = '';
  String _verificationId;
  bool autoLogin = true;
  bool _isButtonDisabled = false;

  var _scaffold = GlobalKey<ScaffoldState>();

//  region Overrides

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController();
    isLoggedIn();

  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {

    SetScreenDimension(context);

    return Scaffold(
        key: _scaffold,
//        appBar: AppBar(title : Text("Login")),
        body : SingleChildScrollView(child: _body())
//        body : Container(
//            width: double.infinity,
//            child: ImageClipper(image: Image.asset(IMAGE_PATH_ROBOT, fit: BoxFit.fill,), cliptype: ClipType(Clip.bottom_wave_clipper),))
    );
  }

//  endregion

//  region Widget

  Widget _body(){
    return Container(
      height: Store.dimension.height,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(child: Center(child: Text('Login', style: Theme.of(context).textTheme.display3,))),
//          Expanded(flex: 3,child: Container(
//              width: double.infinity,
//              child: ImageClipper(image: Image.asset(IMAGE_PATH_ROBOT, fit: BoxFit.cover,), cliptype: ClipType(Clip.bottom_wave_clipper),))),

          Expanded(flex: 1, child: login()),
//            Expanded(flex: 3,child: login()),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: signup(),
          )
        ],
      ),
    );
  }

  Widget header(){
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(LOGIN, textScaleFactor: 3.0,),
        ],
      ),
    );
  }


  Widget login(){

    return FractionallySizedBox(
      heightFactor: 1.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(child: Text(autoLogin ? MOBILE+' '+NUMBER : OTP, style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.blue, fontWeight: FontWeight.w500,fontFamily: FONT_FAMILY_OXYGEN), textScaleFactor: 1.5,)),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(controller: _controller,decoration: InputDecoration(border: InputBorder.none, counterText: ''), enabled: true, keyboardType: autoLogin ? TextInputType.phone : TextInputType.number, textAlign: TextAlign.center, maxLength: autoLogin ? 10 : 6, ),
                Container(
                  height: 1.0,
                  width: Store.dimension.width/3,
                  color: Colors.orange,
                )
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FloatingActionButton(
              onPressed: ((){
                FocusScope.of(context).requestFocus(new FocusNode());

                if(!_isButtonDisabled){
                  autoLogin ? _verifyPhoneNumber() : _signInWithPhoneNumber();
                  _scaffold.currentState.showSnackBar(SnackBar(content: Text('Please wait.'), duration: Duration(seconds: 3),));
                }else{
                  _isButtonDisabled = true;
                }
              }),
              child: Icon(Icons.arrow_forward_ios),
            ),
          )
        ],
      ),
    );
  }

  Widget signup(){

    return HtmlText(text: welcomeToAyeFinance,
        urlStyle: TextStyle(color: Colors.blue),
        onTapUrl: ((url){
          Log.d(tag: TAG_LOGIN, message: 'Url : $url');
          Launcher().launch(element: url);
//      Navigator.pushNamed(context, RoutePaths.WebScreen, arguments: WebScreenScreenArguments(url : url));
        }));
  }


//  endregion




  // region Actions

  void _verifyPhoneNumber() async {

//    setState(() {
//      _message = '';
//    });

    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential phoneAuthCredential) {
//      _auth.signInWithCredential(phoneAuthCredential);
      _verifyLogin(phoneAuthCredential);
      setState(() {
        _message = RECEIVED_PHONE_CREDENTIALS+': $phoneAuthCredential';

//        print(_message);
        _verifyLogin(phoneAuthCredential);
      });
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      setState(() {
        _message =
            PHONE_VERIFIFCATION_FAILED+'. Code: ${authException.code}. Message: ${authException.message}';

      });
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      _scaffold.currentState.showSnackBar(const SnackBar(
        content: Text(CHECK_OTP),
      ));
      _verificationId = verificationId;

      setState(() {

        autoLogin = false;
        _controller.clear();
      });

    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: '+'+COUNTRY_CODE_INDIA+_controller.text,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }


  void _signInWithPhoneNumber() async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: _verificationId,
      smsCode: _controller.text,
    );

    _verifyLogin(credential);
  }

  Future _verifyLogin(AuthCredential credential) async {

    Log.d(tag: TAG_LOGIN, message: 'Credentials : ${credential.toString()}');

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
//    final FirebaseUser currentUser = await _auth.currentUser();
//    assert(user.uid == currentUser.uid);

    if (user != null) {
      _userBoard(user);
      _message = SIGN_IN_SUCCESSFUL+', uid: ' + user.uid;

    } else {
      _message = SIGN_IN_FAILED;
    }

  }





// endregion


  isLoggedIn() async {
    FirebaseUser _user = await FirebaseAuth.instance.currentUser();

    if (_user != null) {
      _userBoard(_user);
    }
//    else{
//      Navigator.pushReplacementNamed(context, RoutePaths.Login);
//    }
  }

  void _userBoard(FirebaseUser user){

    _controller.clear();
    _scaffold.currentState.showSnackBar(SnackBar(
      content: Text(ACCOUNT_VERIFICATION_MSG+', '+PLEASE_WAIT),
    ));

//    Gets all available access
    QueryType().sharedAccess(accessList: ((access){
      Store.access = access;
      access.values.forEach((value){
        if(value.name == 'Employee')
          Store.accessEmployee = value;
        _userProfile(user);
      });
    }));


  }

  _userProfile(FirebaseUser user){
    Store.authUser = user;

    QueryType().userProfile(userId: user.uid, user: ((user){
      try {
        if (user != null) {
          Store.userModel = user;
          navigateToDashboard(user);

        } else {
          Navigator.pushReplacementNamed(context, RouterPath(Path.SignupScreen));
        }
      }catch(e){
        Log.d(tag: TAG_LOGIN, message:'Exception : $e');
      }
    }));
  }


  navigateToDashboard(UserModel value){
    Navigator.pushReplacementNamed(context, RouterPath(Path.HomeScreen));
//    var userAccess = UserModel.fromJson(value.cast<String, dynamic>()).access;
//  print('Access : $userAccess');
//    getData(permissionReference).then((value){
//      if(value[BLOCKED] == userAccess)
////      Todo : Account block screen is to be made and implemented
//        _scaffold.currentState.showSnackBar(SnackBar(
//          content: Text(ACCESS_BLOCKED_MSG),
//        ));
//
//      else if(value[GUARD] == userAccess)
//        Navigator.pushReplacementNamed(context, RoutePaths.DashboardGuard);
//
//      else {
//
//        if(value[ADMIN] == userAccess || value[COUNTER] == userAccess){
//          Store.webAccessEnabled = true;
//        }
//        Navigator.pushReplacementNamed(context, RoutePaths.Dashboard);
//      }
//
//    });
  }

}
