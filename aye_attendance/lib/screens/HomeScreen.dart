

import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/jsonConstants.dart';
import 'package:aye_attendance/constants/ui/fonts.dart';
import 'package:aye_attendance/routes/router.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/Launcher.dart';
import 'package:aye_attendance/utils/QrGenerator.dart';
import 'package:aye_attendance/utils/ScreenOrientation.dart';
import 'package:aye_attendance/utils/ui/clippers/ClipType.dart';
import 'package:aye_attendance/utils/ui/clippers/ImageClipper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

enum actions {
  punch,
  attendance,
  help,
  leaves
}

class _HomeScreenState extends State<HomeScreen> {


  var options = [
    [
      [Icons.center_focus_strong, PUNCH, actions.punch, Colors.redAccent],
      [Icons.calendar_today, ATTENDANCE, actions.attendance, Colors.lightBlue]],
    [
      [Icons.view_list, LEAVES, actions.leaves, Colors.orangeAccent],
      [Icons.help, HELP, actions.help, Colors.cyan]
    ],
  ];

  @override
  void initState() {
    super.initState();
    ScreenOrientation().setPotraitMode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(TITLE_HOME_SCREEN, style: Theme
            .of(context)
            .textTheme
            .headline,)),
        body: _body()
    );
  }

  Widget _body() {
    return Column(
      children: <Widget>[
        Expanded(
            flex: 1,
            child: Container(
              width: double.infinity,
              child: ImageClipper(
                cliptype: ClipType(Clip.sliding_edge_clipper2),
                image: Image.network(
                  jsonConstant.elementAt(0)['image_url'],
                  fit: BoxFit.fill,
//                  color: Colors.black54,
//                  colorBlendMode: BlendMeode.dstATop,
                ),
              ),
            )),
        Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: options.map((value) =>
                    Expanded(
                      child: Row(
                        children: <Widget>[
//                      Expanded(child: Placeholder(color: Colors.blueAccent,),),
//                      Expanded(child: Placeholder(color: Colors.green,),),
                          Expanded(child: Container(
                              height: double.infinity,
                              padding: EdgeInsets.all(8.0),
                              child: InkWell(
                                onTap: (() {
                                  onClick(value[0][2]);
                                }),
                                child: Card(
                                    color: value[0][3],
                                    elevation: 3.0,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          Store.dimension.width / 20),
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment
                                          .center,
                                      children: <Widget>[
                                        Icon(value[0][0], color: Colors.white,
                                          size: Store.dimension.width / 8,),
                                        Text(value[0][1],
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .subhead
                                              .copyWith(color: Colors.white),),
                                      ],
                                    )),
                              ))),
                          Expanded(child: Container(
                              height: double.infinity,
                              padding: EdgeInsets.all(8.0),
                              child: InkWell(
                                onTap: (() {
                                  onClick(value[1][2]);
                                }),
                                child: Card(
                                    color: value[1][3],
                                    elevation: 3.0,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          Store.dimension.width / 20),
                                    ), child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(value[1][0], color: Colors.white,
                                      size: Store.dimension.width / 8,),
                                    Text(value[1][1],
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .subhead
                                          .copyWith(
                                          color: Colors.white),),
                                  ],
                                )),
                              ))),
                        ],
                      ),
                    )).toList(),
              ),
            )),
      ],
    );
  }

  onClick(var action) {
    switch (action) {
      case actions.punch :
        _settingModalBottomSheet();
        break;
      case actions.attendance :
        Navigator.pushNamed(context, RouterPath(Path.AttendanceScreen));
        break;
      case actions.leaves :
        Navigator.pushNamed(context, RouterPath(Path.LeaveScreen));
        break;
      case actions.help :
        Launcher().launch(element: FEEDBACK_URL);
        break;
    }
  }


  _settingModalBottomSheet() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
            color: Colors.transparent,
            margin: EdgeInsets.all(Store.dimension.width/15),
            height: Store.dimension.height/1.5,
            width: double.infinity,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0)
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.only(top: Store.dimension.height/30),
                      alignment: Alignment.center,
                      child: SizedBox(
                          height: Store.dimension.height / 2.5,
                          width: Store.dimension.width / 2.5,
                          child: QrGenerator().generate(data: 'dddd')),
                    ),
                  ),
                  Expanded(child: Center(child: Text('Intime : 08:30 am',style: Theme.of(context).textTheme.subhead,))),
                  Expanded(
                    flex: 1,
                    child: SizedBox.expand(
                      child: MaterialButton(
                      color: Colors.green
                      ,onPressed: ((){
                        Navigator.pop(context);
                      }),child: Text('Close', style: Theme.of(context).textTheme.subhead.copyWith(fontFamily: FONT_FAMILY_OXYGEN, color: Colors.white, letterSpacing: 1.0),),),
                    ),
                  )
                ],
              ),
            ),
          );
        }
    );
  }
}