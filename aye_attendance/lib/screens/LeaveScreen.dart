
import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/constants/jsonConstants.dart';
import 'package:aye_attendance/constants/ui/fonts.dart';
import 'package:aye_attendance/models/leaveModel.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/DateFormatter.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:flutter/material.dart';

class LeaveScreen extends StatefulWidget {
  @override
  _LeaveScreenState createState() => _LeaveScreenState();
}

class _LeaveScreenState extends State<LeaveScreen> {


  @override
  void initState() {
    super.initState();

    List<LeaveModel> leaves = [];

    Log.d(tag: TAG_LEAVES, message: dateTimeJsonConstant.toString());

    dateTimeJsonConstant.forEach((elem){
//      Log.d(tag: TAG_LEAVES, message: 'Elem : ${elem.toString()}');
      leaves.add(LeaveModel.fromJson(elem));
    });

    Store.leaves = leaves;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(title : Text("Leaves", style: Theme.of(context).textTheme.headline)),
        body : _body()
    );
  }

  Widget _body(){
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(flex: 1,
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _balanceLeavesHeader(
                      'Leaves',
                      'Total',
                      'Available',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _balanceLeaves(
                      leavesStructure['rh_title'],
                      leavesStructure['rh_initial'].toString(),
                      leavesStructure['rh_final'].toString(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _balanceLeaves(
                      leavesStructure['cl_title'],
                      leavesStructure['cl_initial'].toString(),
                      leavesStructure['cl_final'].toString(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _balanceLeaves(
                      leavesStructure['sl_title'],
                      leavesStructure['sl_initial'].toString(),
                      leavesStructure['sl_final'].toString(),
                    ),
                  ),

                ],
              ),
            ),),
          Expanded(
            flex: 2,
            child: ListView.builder(
              itemCount: Store.leaves.length,
              itemBuilder: (_, index)=> _leave(Store.leaves.elementAt(index)),
            ),
          ),
        ],
      ),
    );
  }

  Widget _leave(LeaveModel leaveModel){
    return Container(
      height: Store.dimension.height/9,
      child: Card(
//      color: Colors.transparent,
        child: ListTile(
          leading: Container(
            height: double.infinity,
            width: Store.dimension.width/10,
            child: CircleAvatar(
                minRadius: Store.dimension.height/8,
                child: Text(leaveModel.code, style: Theme.of(context).textTheme.subtitle.copyWith(letterSpacing: 1.0, color: Colors.white),),
                backgroundColor: codeColor(leaveModel.code)),
          ),
          title: Text(ddMMyyyy_DateFormat(value: dateFromTimestamp(timestamp: leaveModel.timestamp*1000)), style: Theme.of(context).textTheme.headline,),
          subtitle: Text(leaveModel.title),
        ),
      ),
    );
  }


  Color codeColor(String code){

    switch(code){

      case 'CL' :
        return Colors.redAccent;
      case 'SL' :
        return Colors.orange;
      case 'RH' :
        return Colors.indigoAccent;

    }
  }


  Widget _balanceLeaves(String key, String initialValue, String finalValue){
    return Row(
      children: <Widget>[
        Expanded(flex: 2, child: Text(key, style: Theme.of(context).textTheme.subhead,)),
        Expanded(flex: 1, child: Text(initialValue, textAlign: TextAlign.center,)),
        Expanded(flex: 1, child: Text(finalValue, textAlign: TextAlign.center,)),
      ],
    );
  }
  Widget _balanceLeavesHeader(String key, String initialValue, String finalValue){

    var theme = Theme.of(context).textTheme.subhead.copyWith(fontWeight: FontWeight.w600, fontFamily: FONT_FAMILY_OXYGEN);

    return Row(
      children: <Widget>[
        Expanded(flex: 2, child: Text(key, style: theme)),
        Expanded(flex: 1, child: Text(initialValue, style: theme)),
        Expanded(flex: 1, child: Text(finalValue, style: theme,)),
      ],
    );
  }
}
