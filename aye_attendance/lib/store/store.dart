

import 'package:aye_attendance/models/leaveConfig.dart';
import 'package:aye_attendance/models/leaveModel.dart';
import 'package:aye_attendance/models/sharedAccess.dart';
import 'package:aye_attendance/models/userModel.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../utils/ui/SetScreenDimension.dart';

// @required   firebase_auth: ^0.14.0+5
// @required  firebase_database: ^3.0.7

class Store{

 static FirebaseUser _authUser;

 static FirebaseUser get authUser => _authUser;

 static set authUser(FirebaseUser value) {
   _authUser = value;
 }

 static UserModel _userModel;

 static UserModel get userModel => _userModel;

 static set userModel(UserModel value) {
  _userModel = value;
 }

 static List<LeaveModel> _leaves;

 static List<LeaveModel> get leaves => _leaves;

 static set leaves(List<LeaveModel> value) {
  _leaves = value;
 }

 static Map<String,SharedAccess> _access;


 static Map<String, SharedAccess> get access => _access;

 static set access(Map<String, SharedAccess> value) {
  _access = value;
 }

 static SharedAccess _accessEmployee;

 static SharedAccess get accessEmployee => _accessEmployee;

 static set accessEmployee(SharedAccess value) {
  _accessEmployee = value;
 }


 static List<LeaveConfig> _leaveConfig;

 static List<LeaveConfig> get leaveConfig => _leaveConfig;

 static set leaveConfig(List<LeaveConfig> value) {
  _leaveConfig = value;
 }

 // static bool _webAccessEnabled = false;
//
// static bool get webAccessEnabled => _webAccessEnabled;
//
// static set webAccessEnabled(bool value) {
//   _webAccessEnabled = value;
// }

 static Dimension _dimension;

 static Dimension get dimension => _dimension;

 static set dimension(Dimension value) {
   _dimension = value;
 }
}