
import 'package:aye_attendance/constants/constant.dart';
import 'package:aye_attendance/screens/AttendanceScreen.dart';
import 'package:aye_attendance/screens/HomeScreen.dart';
import 'package:aye_attendance/screens/LeaveScreen.dart';
import 'package:aye_attendance/screens/LoginScreen.dart';
import 'package:aye_attendance/screens/SignupScren.dart';
import 'package:aye_attendance/store/store.dart';
import 'package:aye_attendance/utils/Logger.dart';
import 'package:flutter/material.dart';

enum Path{
  HomeScreen,
  LoginScreen,
  AttendanceScreen,
  LeaveScreen,
  SignupScreen,
  ScreenNotAvailable
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    Log.d(tag: TAG_ROUTER, message: 'Route Name : ${settings.name}');

    switch (path(settings.name)) {
//      case RoutePaths.Welcome:
//        return MaterialPageRoute(builder: (_) => Welcome());
//        break;
      case Path.LoginScreen:
        return MaterialPageRoute(builder: (_) => LoginScreen());
        break;

      case Path.AttendanceScreen:
        return MaterialPageRoute(builder: (_) => AttendanceScreen());
        break;
      case Path.LeaveScreen:
        return MaterialPageRoute(builder: (_) => LeaveScreen());
        break;

      case Path.SignupScreen:
        return MaterialPageRoute(builder: (_) => SignupScreen());
        break;

//      case RoutePaths.Signup:
//        return MaterialPageRoute(builder: (_) => SignUp());
//        break;
//      case RoutePaths.Dashboard:
//        return MaterialPageRoute(builder: (_) => Dashboard());
//        break;
//      case RoutePaths.DashboardGuard:
//        return MaterialPageRoute(builder: (_) => DashboardGuard());
//        break;
//      case RoutePaths.UpdateProfile:
//        return MaterialPageRoute(builder: (_) => UpdateProfile());
//        break;
      case Path.HomeScreen:
        return MaterialPageRoute(builder: (_) => HomeScreen());
        break;
//      case RoutePaths.HomeScreen2:
//        return MaterialPageRoute(builder: (_) => HomeScreen2());
//        break;
//
//      case RoutePaths.HistoryScreen:
//        return MaterialPageRoute(builder: (_) => HistoryScreen());
//        break;
//      case RoutePaths.WebScreen:
//        final WebScreenScreenArguments args = settings.arguments;
//        return MaterialPageRoute(builder: (_) => WebScreen(url: args.url,));
//        break;

      default:
        Log.d(tag: TAG_ROUTER, message: '${settings.name} not availabel.');
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('You are not allowed to Access this Screen.'),
              ),
            ));
    }
  }
}

class WebScreenScreenArguments {

  String url;

  WebScreenScreenArguments({this.url});
}


// region RoutePaths


String RouterPath(Path p){

  if(Store.access != null && Store.userModel != null){
    var _allowedScreens = Store.access[Store.userModel.access].screens;
    if(_allowedScreens.contains(p.toString().replaceAll('Path.', ''))){
      return p.toString();
    }
  }

  var temp = [Path.LoginScreen, Path.SignupScreen];
  if(temp.contains(p))
    return p.toString();

  return Path.ScreenNotAvailable.toString();
}


path(String name){
  Path screen = Path.ScreenNotAvailable;

  for(int i = 0; i< Path.values.length; i++){
    var temp = Path.values.elementAt(i);
    if(temp.toString() == name){
      screen = temp;
      break;
    }
  }

  return screen;
}

// endregion
