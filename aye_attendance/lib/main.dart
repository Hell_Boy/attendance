import 'package:aye_attendance/routes/router.dart';
import 'package:flutter/material.dart';
import 'constants/ui/fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aye Attendance',
      theme: ThemeData(
//        primaryColor: const Color(0xff4cb26e),
//        primaryColorLight: const Color(0xff7fe59d),
//        primaryColorDark: const Color(0xff098242),
//
          primaryColor: const Color(0xfffbffff),
          primaryColorLight: const Color(0xffffffff),
//        primaryColorDark: const Color(0xffc8cccc),
          primaryColorDark: const Color(0xfffbffff),
//        primarySwatch: Colors.green,

          appBarTheme: AppBarTheme(elevation: 0.0, color: const Color(0xfffbffff), brightness: Brightness.light),
          accentColor: Colors.green,
          brightness: Brightness.light,
          fontFamily: FONT_FAMILY_ZCOOL

      ),
//      home: HomeScreen(),
      initialRoute: RouterPath(Path.LoginScreen),
      onGenerateRoute: Router.generateRoute,
    );
  }
}

